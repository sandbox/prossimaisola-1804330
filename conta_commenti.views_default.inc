<?php

/**
 * Implementation of hook_views_default_views().
 */
function conta_commenti_views_default_views() {
  $views = array();

  // Exported view: conta_commenti
    $view = new view;
    $view->name = 'conta_commenti';
    $view->description = '';
    $view->tag = '';
    $view->view_php = '';
    $view->base_table = 'comments';
    $view->is_cacheable = FALSE;
    $view->api_version = 2;
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->override_option('fields', array(
    'name' => array(
        'label' => 'Ruolo',
        'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_user' => 0,
        'exclude' => 0,
        'id' => 'name',
        'table' => 'comments',
        'field' => 'name',
        'relationship' => 'none',
    ),
    'cid' => array(
        'label' => 'Commenti',
        'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_comment' => 0,
        'exclude' => 0,
        'id' => 'cid',
        'table' => 'comments',
        'field' => 'cid',
        'relationship' => 'none',
    ),
    'views_sql_groupedfields' => array(
        'label' => 'Group By Fields',
        'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'alt' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'exclude' => '1',
        'id' => 'views_sql_groupedfields',
        'table' => 'views_groupby',
        'field' => 'views_sql_groupedfields',
        'relationship' => 'none',
        'views_groupby_fields_to_group' => array(
        'name' => 'name',
        ),
        'views_groupby_sql_function' => 'count',
        'views_groupby_fields_to_aggregate' => array(
        'cid' => 'cid',
        ),
        'views_groupby_field_sortby' => 'cid',
        'views_groupby_sortby_direction' => 'asc',
    ),
    ));
    $handler->override_option('filters', array(
    'timestamp' => array(
        'operator' => '=',
        'value' => array(
        'min' => '',
        'max' => '',
        'value' => '',
        ),
        'group' => '0',
        'exposed' => TRUE,
        'expose' => array(
        'use_operator' => FALSE,
        'operator' => 'timestamp_op',
        'identifier' => 'timestamp',
        'label' => 'Commenti: Post date',
        'remember' => FALSE,
        'single' => TRUE,
        'optional' => TRUE,
        ),
        'id' => 'timestamp',
        'table' => 'comments',
        'field' => 'timestamp',
    ),
    ));
    $handler->override_option('access', array(
    'type' => 'none',
    ));
    $handler->override_option('cache', array(
    'type' => 'none',
    ));
    $handler->override_option('use_ajax', TRUE);
    $handler->override_option('items_per_page', 0);
    $handler->override_option('use_pager', '0');
    $handler->override_option('style_plugin', 'table');
    $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
        'cid' => 'cid',
        'name' => 'name',
        'views_sql_groupedfields' => 'views_sql_groupedfields',
    ),
    'info' => array(
        'cid' => array(
        'sortable' => 0,
        'separator' => '',
        ),
        'name' => array(
        'sortable' => 0,
        'separator' => '',
        ),
        'views_sql_groupedfields' => array(
        'separator' => '',
        ),
    ),
    'default' => '-1',
    ));
    $handler = $view->new_display('block', 'Conteggio commenti', 'block_1');
    $handler->override_option('filters', array(
    'timestamp' => array(
        'operator' => '>=',
        'value' => array(
        'type' => 'date',
        'value' => '',
        'min' => '',
        'max' => '',
        ),
        'group' => '0',
        'exposed' => TRUE,
        'expose' => array(
        'use_operator' => 0,
        'operator' => 'timestamp_op',
        'identifier' => 'timestamp',
        'label' => 'Da:',
        'optional' => 1,
        'remember' => 0,
        ),
        'id' => 'timestamp',
        'table' => 'comments',
        'field' => 'timestamp',
        'override' => array(
        'button' => 'Use default',
        ),
        'relationship' => 'none',
    ),
    'timestamp_1' => array(
        'operator' => '<=',
        'value' => array(
        'type' => 'date',
        'value' => '',
        'min' => '',
        'max' => '',
        ),
        'group' => '0',
        'exposed' => TRUE,
        'expose' => array(
        'use_operator' => 0,
        'operator' => 'timestamp_1_op',
        'identifier' => 'timestamp_1',
        'label' => 'A:',
        'optional' => 1,
        'remember' => 0,
        ),
        'id' => 'timestamp_1',
        'table' => 'comments',
        'field' => 'timestamp',
        'override' => array(
        'button' => 'Use default',
        ),
        'relationship' => 'none',
    ),
    ));
    $handler->override_option('title', 'Conteggio commenti');
    $handler->override_option('block_description', '');
    $handler->override_option('block_caching', -1);


  $views[$view->name] = $view;

  return $views;
}
